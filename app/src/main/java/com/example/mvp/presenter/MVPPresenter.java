package com.example.mvp.presenter;

import com.example.mvp.model.MVPModel;
import com.example.mvp.view.MVPView;

public class MVPPresenter {

    private final MVPModel model;
    private final MVPView view;

    public MVPPresenter(MVPView view) {
        this.model = new MVPModel();
        this.view = view;
    }

    public void addData() {
        String data = view.getData();

        if (data.isEmpty())
            view.showDataError();
        else
        if(model.doSomething(data))
            view.showDataSucces();
        else
            view.showDataError();

    }

}
