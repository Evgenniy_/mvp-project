package com.example.mvp.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mvp.R;
import com.example.mvp.presenter.MVPPresenter;

public class MainActivity extends AppCompatActivity implements MVPView {
    private MVPPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new MVPPresenter(this);
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addData();
            }
        });
    }

    @Override
    public void showDataSucces() {
        showMsg("Успешный ввод");
    }

    @Override
    public void showDataError() {
        showMsg("Ошибка ввода данных");
    }

    private void showMsg(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public String getData() {
        return ((EditText)this.findViewById(R.id.inputText)).getText().toString();
    }
}