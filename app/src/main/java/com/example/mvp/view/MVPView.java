package com.example.mvp.view;

public interface MVPView {
    void showDataSucces();
    void showDataError();
    String getData();
}